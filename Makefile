CC = gcc
DESTDIR = /usr/local/bin

traytool: main.c
	$(CC) -Wall -g -fsanitize=address,leak,undefined -o traytool main.c -lsystemd

traytool-release: main.c
	$(CC) -Wall -O3 -o traytool-release main.c -lsystemd

.PHONY: run install
run: traytool
	@./traytool

install: traytool-release
	install -m755 traytool-release -D $(DESTDIR)/traytool
