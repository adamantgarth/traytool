#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <getopt.h>
#include <unistd.h>
#include <systemd/sd-bus.h>

#define finish(label, ...) do { fprintf(stderr, __VA_ARGS__); goto label; } while(0)

#define PARSE_ASSERT(ret) do { if (ret < 0) { fprintf(stderr, "Error: parsing error at line %d: %s\n", __LINE__, strerror(-ret)); return false; } } while(0)
#define MENU_MAX_PARENTS_LENGTH 1024
#define MENU_MAX_LABEL_LENGTH 256

// Enforced by protocol
#define DBUS_MAX_NAME_LEN 256

typedef enum {GRAY, RED, GREEN, YELLOW, RESET} Color;
const char *ANSI_COLORS[] = {
    [GRAY]   = "\033[90m",
    [RED]    = "\033[91m",
    [GREEN]  = "\033[92m",
    [YELLOW] = "\033[93m",
    [RESET]  = "\033[m",
};
char **COLORS = (char *[]) {"", "", "", "", ""};

static const char *DEFAULT_MENU_COMMAND[] = {"fzf", "-d\t", "--with-nth=2..", "--ansi", "--reverse", "--info=hidden", "--tiebreak=index", NULL};

// Get the list of registered StatusNotifierItems as a NULL-terminated array of strings
bool getRegisteredItems(sd_bus *bus, char ***items) {
    sd_bus_error error = SD_BUS_ERROR_NULL;
    int ret = sd_bus_get_property_strv(bus, "org.kde.StatusNotifierWatcher", "/StatusNotifierWatcher", "org.kde.StatusNotifierWatcher", "RegisteredStatusNotifierItems", &error, items);
    if (ret < 0) fprintf(stderr, "Error: failed to get the list of StatusNotifier items: %s\n", error.message);
    sd_bus_error_free(&error);
    return ret < 0 ? false : true;
}

static inline void freeRegisteredItems(char **items) {
    for (int i = 0; items[i] != NULL; ++i)
        free(items[i]);
    free(items);
}

// Get StatusNotifierItem's Id property
bool getId(sd_bus *bus, char *name, char *path, char **id) {
    sd_bus_error error = SD_BUS_ERROR_NULL;
    int ret = sd_bus_get_property_string(bus, name, path, "org.kde.StatusNotifierItem", "Id", &error, id);
    if (ret < 0) fprintf(stderr, "Error: couldn't get the property 'Id' of '%s%s': %s\n", name, path, error.message);
    sd_bus_error_free(&error);
    return ret < 0 ? false : true;
}

// Split destination+path
static inline bool splitAddress(const char *address, char name[DBUS_MAX_NAME_LEN], char **path) {
    *path = strchr(address, '/');
    size_t name_len = *path - address;
    if (name_len < 1) {
        fprintf(stderr, "Error: malformed StatusNotifierItem address: %s%s\n", name, *path);
        return false;
    }
    memcpy(name, address, name_len);
    name[name_len] = 0;
    return true;
}

bool findItemById(sd_bus *bus, const char *needle, char **haystack, char name[DBUS_MAX_NAME_LEN], char **path) {
    if (!haystack)
        return false;
    bool found = false;
    for (int i = 0; haystack[i] != NULL; ++i) {
        if (!splitAddress(haystack[i], name, path))
            return false;
        char *id;
        if (!getId(bus, name, *path, &id))
            return false;
        if (strcmp(needle, id) == 0) {
            found = true;
            free(id);
            break;
        }
        free(id);
    }
    return found;
}

bool printItemIds(sd_bus *bus) {
    char **items;
    if (!getRegisteredItems(bus, &items)) goto end;

    char name[DBUS_MAX_NAME_LEN];
    char *path;

    printf("Available StatusNotifierItems:\n");
    for (int i = 0; items[i] != NULL; ++i) {
        if (!splitAddress(items[i], name, &path))
            continue;
        char *id;
        if (!getId(bus, name, path, &id))
            continue;
        printf("* %s\n", id);
        free(id);
    }
end:
    freeRegisteredItems(items);
    return true;
}

// Forks a child, optionally creating pipes for communicating with it,
// if the in or out arguments aren't NULL. The return value is child's pid.
// If an error happened - the return value is 0 from child and -1 from parent. 
pid_t subprocess(char **argv, FILE **in, FILE **out) {
    int pipe_in[2], pipe_out[2];

    if (in != NULL)
        if (pipe(pipe_in) < 0) finish(pipe_in, "Error: failed to create input pipe for the child\n");

    if (out != NULL)
        if (pipe(pipe_out) < 0) finish(pipe_out, "Error: failed to create output pipe for the child\n");

    pid_t child_pid;
    switch (child_pid = fork()) {
    case -1:
        // Error
        finish(fork, "Error: fork failed: %s\n", strerror(errno));
    case 0:
        // Child
        if (in != NULL) {
            close(pipe_in[1]);
            if (dup2(pipe_in[0], STDIN_FILENO) < 0)
                finish(child, "Error: failed to redirect child's stdin: %s\n", strerror(errno));
        }
        if (out != NULL) {
            close(pipe_out[0]);
            if (dup2(pipe_out[1], STDOUT_FILENO) < 0)
                finish(child, "Error: failed to redirect child's stdout: %s\n", strerror(errno));
        }

        execvp(argv[0], argv);
        fprintf(stderr, "Error: fork failed: %s\n", strerror(errno));

    child:
        if (in != NULL) close(pipe_in[0]);
        if (out != NULL) close(pipe_out[0]);
        return 0;
    }

    // Parent
    if (in != NULL) {
        close(pipe_in[0]);
        if ((*in = fdopen(pipe_in[1], "w")) == NULL)
            finish(fdopen, "Error: could't open child's input pipe: %s\n", strerror(errno));
    }
    if (out != NULL) {
        close(pipe_out[1]);
        if ((*out = fdopen(pipe_out[0], "r")) == NULL)
            finish(fdopen, "Error: could't open child's output pipe: %s\n", strerror(errno));
    }
    return child_pid;

fdopen:
    if (in != NULL) close(pipe_in[1]);
    if (out != NULL) close(pipe_out[0]);
    return -1;

fork:
    if (out != NULL) {
        close(pipe_out[0]);
        close(pipe_out[1]);
    }
pipe_out:
    if (in != NULL) {
        close(pipe_in[0]);
        close(pipe_in[1]);
    }
pipe_in:
    return -1;
}

//
// Activation
//

bool activateItem(sd_bus *bus, const char *target, const bool is_addr, char **argv) {
    int ret = -ESRCH;
    char **items = NULL;
    char name[DBUS_MAX_NAME_LEN];
    char *path;
    if (is_addr) {
        if (!splitAddress(target, name, &path))
            return false;
    } else {
        if (!getRegisteredItems(bus, &items))
            goto fallback;
        if (!findItemById(bus, target, items, name, &path))
            goto items;
    }
    
    sd_bus_error error = SD_BUS_ERROR_NULL;
    sd_bus_message *reply = NULL;
    ret = sd_bus_call_method(bus, name, path, "org.kde.StatusNotifierItem", "Activate", &error, &reply, "ii", 0, 0);

    sd_bus_message_unref(reply);
    sd_bus_error_free(&error);

items:
    if (!is_addr)
        freeRegisteredItems(items);
    if (ret >= 0)
        return true;
fallback:
    if (argv != NULL) {
        fprintf(stderr, "Failed to activate '%s', running fallback command...\n", target);
        pid_t pid;
        switch (pid = subprocess(argv, NULL, NULL)) {
        case -1:
            fprintf(stderr, "Error: running fallback command failed\n");
            // fallrhrough
        case 0:
            return false;
        }
        int status = 69;
        if (waitpid(pid, &status, 0) == -1) {
            fprintf(stderr, "Error: couldn't wait for fallback command: %s\n", strerror(errno));
            return false;
        }
        if (WIFEXITED(status)) {
            if (WEXITSTATUS(status) == 0)
                return true;
            else
                fprintf(stderr, "Warning: fallback command exited with status %d\n", WEXITSTATUS(status));
            return false;
        }
        fprintf(stderr, "Warning: fallback command exited unexpectedly\n");
    } else {
        fprintf(stderr, "Error: failed to activate '%s': %s\n", target, strerror(-ret));
    }
    return true;
}

//
// Menu
//

bool renderMenu(sd_bus_message *msg, FILE *output, char *parents, size_t parents_len) {
    // Enter the struct
    PARSE_ASSERT(sd_bus_message_enter_container(msg, SD_BUS_TYPE_STRUCT, "ia{sv}av"));
    
    // Read the id
    int32_t id = 0;
    PARSE_ASSERT(sd_bus_message_read_basic(msg, 'i', &id));

    // Start reading the properties
    PARSE_ASSERT(sd_bus_message_enter_container(msg, SD_BUS_TYPE_ARRAY, "{sv}"));

    char *type = NULL;
    char *raw_label = NULL;
    int enabled = true;
    int visible = true;
    char *toggle_type = NULL;
    int32_t toggle_state = -1;
    char *children_display = NULL;

    for (;;) {
        int ret = sd_bus_message_enter_container(msg, SD_BUS_TYPE_DICT_ENTRY, "sv");
        if (ret == 0) break;
        PARSE_ASSERT(ret);

        // Read the property key
        char *key;
        PARSE_ASSERT(sd_bus_message_read_basic(msg, 's', &key));

        // Read the property value
        PARSE_ASSERT(sd_bus_message_enter_container(msg, SD_BUS_TYPE_VARIANT, NULL));

        if (strcmp(key, "type") == 0)
            PARSE_ASSERT(sd_bus_message_read_basic(msg, 's', &type));
        else if (strcmp(key, "label") == 0)
            PARSE_ASSERT(sd_bus_message_read_basic(msg, 's', &raw_label));
        else if (strcmp(key, "enabled") == 0)
            PARSE_ASSERT(sd_bus_message_read_basic(msg, 'b', &enabled));
        else if (strcmp(key, "visible") == 0)
            PARSE_ASSERT(sd_bus_message_read_basic(msg, 'b', &visible));
        else if (strcmp(key, "toggle-type") == 0)
            PARSE_ASSERT(sd_bus_message_read_basic(msg, 's', &toggle_type));
        else if (strcmp(key, "toggle-state") == 0)
            PARSE_ASSERT(sd_bus_message_read_basic(msg, 'i', &toggle_state));
        else if (strcmp(key, "children-display") == 0)
            PARSE_ASSERT(sd_bus_message_read_basic(msg, 's', &children_display));
        else
            PARSE_ASSERT(sd_bus_message_skip(msg, NULL));

        PARSE_ASSERT(sd_bus_message_exit_container(msg)); // Variant
        PARSE_ASSERT(sd_bus_message_exit_container(msg)); // Dictionary entry
    }

    // Exit properties array
    PARSE_ASSERT(sd_bus_message_exit_container(msg));

    // Fix labels
    static char label[MENU_MAX_LABEL_LENGTH];
    size_t label_len = 0;
    if (raw_label) {
        for (int i = 0; raw_label[i] != 0 && label_len < MENU_MAX_LABEL_LENGTH; ++i) {
            if (raw_label[i] != '_')
                label[label_len++] = raw_label[i];
            else if (raw_label[i+1] == '_')
                label[label_len++] = raw_label[++i];
        }
        label[label_len] = 0;
    }

    if (children_display) {
        PARSE_ASSERT(sd_bus_message_enter_container(msg, SD_BUS_TYPE_ARRAY, "v"));
        int ret = 0;
        while ((ret = sd_bus_message_enter_container(msg, SD_BUS_TYPE_VARIANT, "(ia{sv}av)")) != 0) {
            PARSE_ASSERT(ret);
            if (raw_label) { 
                PARSE_ASSERT(parents_len + label_len + sizeof(" > ") <= MENU_MAX_PARENTS_LENGTH ? 0 : -EMSGSIZE);
                memcpy(parents + parents_len, label, label_len);
                memcpy(parents + parents_len + label_len, " > ", sizeof(" > "));
                parents_len += label_len + sizeof(" > ");
            }
            if (!renderMenu(msg, output, parents, parents_len)) // Recurse
                return false;
            parents[parents_len] = 0; // Discard everything written into the buffer during recursion
            PARSE_ASSERT(sd_bus_message_exit_container(msg)); // Variant
        }
        PARSE_ASSERT(sd_bus_message_exit_container(msg)); // Array
        goto end;
    }
    else {
        PARSE_ASSERT(sd_bus_message_skip(msg, "av"));
    }

    // Render the menu
    if (!visible)
        goto end;

    if (type && strcmp(type, "separator") == 0) {
        fprintf(output, "\t%s%s----------%s\n", COLORS[GRAY], parents, COLORS[RESET]);
        goto end;
    }
    if (!raw_label)
        goto end;

    if (!enabled) {
        fprintf(output, "\t%s%s<%s>%s\n", COLORS[GRAY], parents, label, COLORS[RESET]);
        goto end;
    }

    if (toggle_type) {
        char *color = COLORS[YELLOW];
        char toggle[5] = {0, '~', 0, ' ', 0};
        if (strcmp(toggle_type, "checkmark") == 0) {
            toggle[0] = '[';
            toggle[2] = ']';
        } else if (strcmp(toggle_type, "radio") == 0) {
            toggle[0] = '(';
            toggle[2] = ')';
        }
        switch (toggle_state) {
        case 0:
            color = COLORS[RED];
            toggle[1] = ' ';
            break;
        case 1:
            toggle[1] = 'x';
            color = COLORS[GREEN];
            break;
        }
        fprintf(output, "%d\t%s%s%s%s%s%s%s\n", id, COLORS[GRAY], parents, COLORS[RESET], color, toggle, label, COLORS[RESET]);
        goto end;
    }

    fprintf(output, "%d\t%s%s%s%s\n", id, COLORS[GRAY], parents, COLORS[RESET], label);

end:
    PARSE_ASSERT(sd_bus_message_exit_container(msg));
    return true;
}

bool showMenu(sd_bus *bus, const char *target, const bool is_addr, char **argv) {
    int ret = -ESRCH;
    char **items = NULL;
    char name[DBUS_MAX_NAME_LEN];
    char *path;
    if (is_addr) {
        if (!splitAddress(target, name, &path))
            return false;
    } else {
        if (!getRegisteredItems(bus, &items))
            return false;
        if (!findItemById(bus, target, items, name, &path)) {
            fprintf(stderr, "Error: StatusNotifierItem '%s' not found\n", target);
            goto items;
        }
    }
    
    // Find the menu path
    sd_bus_error path_error = SD_BUS_ERROR_NULL;
    sd_bus_message *path_msg = NULL;
    ret = sd_bus_get_property(bus, name, path, "org.kde.StatusNotifierItem", "Menu", &path_error, &path_msg, "o");
    if (ret < 0) finish(path, "Error: failed to find the menu location of %s: %s\n", name, path_error.message);

    // Extract the menu path
    char *menu_path = NULL;
    ret = sd_bus_message_read_basic(path_msg, 'o', &menu_path);
    if (ret < 0) finish(path, "Error: failed to decode the menu location of %s: %s\n", name, strerror(-ret));

    // Send a heads up that we're about to show the menu
    sd_bus_error headsup_error = SD_BUS_ERROR_NULL;
    ret = sd_bus_call_method(bus, name, menu_path, "com.canonical.dbusmenu", "AboutToShow", &headsup_error, NULL, "i", 0);
    if (ret < 0) finish(headsup, "Error: failed to notify %s that we're about to display its menu: %s\n", target, headsup_error.message);

    // Get the menu layout
    sd_bus_error menu_error = SD_BUS_ERROR_NULL;
    sd_bus_message *menu_msg = NULL;
    ret = sd_bus_call_method(bus, name, menu_path, "com.canonical.dbusmenu", "GetLayout", &menu_error, &menu_msg, "iias", 0, -1, 0);
    if (ret < 0) finish(menu, "Error: failed to get the menu layout of %s: %s\n", target, menu_error.message);

    // Skip the api version
    ret = sd_bus_message_read_basic(menu_msg, 'u', NULL);
    if (ret < 0) finish(menu, "Error: failed to decode the menu layout of %s: %s\n", target, strerror(-ret));

    // Start the menu selector
    FILE *cmd_in, *cmd_out;
    pid_t pid = -1;
    switch (pid = subprocess(argv, &cmd_in, &cmd_out)) {
    case -1:
        fprintf(stderr, "Error: running menu command failed\n");
        fclose(cmd_in);
        fclose(cmd_out);
        goto fork;
    case 0:
        goto menu;
    }

    // Render the menu
    char parents[MENU_MAX_PARENTS_LENGTH];
    parents[0] = 0;
    if (!renderMenu(menu_msg, cmd_in, parents, 0))
        ret = -1;
    fclose(cmd_in);

    // Read the output
    uint32_t action;
    int n = fscanf(cmd_out, "%u", &action);
    if (n < 1)
        printf("No action\n");
    else {
        // Send the selected action to the StatusNotifierItem
        sd_bus_error click_error = SD_BUS_ERROR_NULL;
        ret = sd_bus_call_method(bus, name, menu_path, "com.canonical.dbusmenu", "Event", &menu_error, NULL, "isvu", action, "clicked", "s", "", (uint32_t) time(NULL));
        if (ret < 0) fprintf(stderr, "Error: failed to send the selected action to %s: %s\n", target, click_error.message);
        sd_bus_error_free(&click_error);
    }

    fclose(cmd_out);

fork:
    // Wait for the menu command
    if (pid > 0) {
        int status = 69;
        if (waitpid(pid, &status, 0) == -1) {
            fprintf(stderr, "Error: couldn't wait for menu command: %s\n", strerror(errno));
            ret = -1;
        }
    }
menu:
    sd_bus_message_unref(menu_msg);
    sd_bus_error_free(&menu_error);
headsup:
    sd_bus_error_free(&headsup_error);
path:
    sd_bus_message_unref(path_msg);
    sd_bus_error_free(&path_error);
items:
    if (!is_addr) freeRegisteredItems(items);
    return ret < 0 ? false : true;
}

static const char *usage =
    "Usage: %s [options] [-- custom command]\n\n"
    "  -h, --help             Show help message and exit\n"
    "  -l, --list             List available StatusNotifierItems (default)\n"
    "  -a, --activate <item>  Activate the specified StatusNotifierItem\n"
    "  -m, --menu <item>      Show the menu of a specified StatusNotifierItem (using fzf)\n"
    "  -c, --colors           Enable ANSI color codes in menu output\n"
    "  -d, --address          Indicate that provided <item> is a D-Bus destination+path, not an id\n\n"
    "Custom command can be provided after `--` to either:\n"
    "  * Run it if --activate fails\n"
    "  * Use it instead of fzf to show the --menu\n\n";

static const struct option long_opts[] = {
    {"help", no_argument, NULL, 'h'},
    {"list", required_argument, NULL, 'l'},
    {"activate", required_argument, NULL, 'a'},
    {"menu", required_argument, NULL, 'm'},
    {"colors", no_argument, NULL, 'c'},
    {"address", no_argument, NULL, 'd'},
};

int main(int argc, char **argv) {
    char action = 'l';
    char *target = NULL;
    char **command = NULL;
    bool is_addr = false;

    if (argc > 1) {
        int opt = 0;
        while ((opt = getopt_long(argc, argv, "hla:m:cd", long_opts, NULL)) != -1) {
            switch (opt) {
            case 'h':
                printf(usage, argv[0]);
                return EXIT_SUCCESS;
                break;
            case 'l':
                action = opt;
                break;
            case 'a':
                action = opt;
                target = optarg;
                break;
            case 'm':
                action = opt;
                target = optarg;
                command = (char **) DEFAULT_MENU_COMMAND;
                break;
            case 'c':
                COLORS = (char **) ANSI_COLORS;
                break;
            case 'd':
                is_addr = true;
                break;
            default:
                fprintf(stderr, usage, argv[0]);
                return EXIT_FAILURE;
            }
        }
        if (argv[optind] != NULL)
            command = argv + optind;
    }

    bool result = false;
    sd_bus *bus = NULL;
    int ret = sd_bus_open_user(&bus);
    if (ret < 0) finish(end, "Error: failed to open DBus connection: %s\n", strerror(-ret));

    switch (action) {
    case 'l':
        result = printItemIds(bus);
        break;
    case 'a':
        result = activateItem(bus, target, is_addr, command);
        break;
    case 'm':
        result = showMenu(bus, target, is_addr, command);
    }

end:
    sd_bus_unref(bus);
    return result ? EXIT_SUCCESS : EXIT_FAILURE;
}
